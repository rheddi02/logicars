const mix = require('laravel-mix');

mix.webpackConfig({
   resolve:{
      extensions: ['.js', '.vue'],
      alias: {
         '@backoffice': path.resolve(__dirname, 'resources/backoffice/js'),
         '@frontoffice': path.resolve(__dirname, 'resources/frontoffice/js')
      }
   }
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

if (process.env.NODE_ENV == 'production'){

   mix.setPublicPath('public');

} else if (process.env.NODE_ENV == 'development'){

   mix.setPublicPath('public/dev');

}

   mix
      .js('resources/backoffice/js/app.js', 'backoffice/js')
      .sass('resources/backoffice/sass/app.scss', 'backoffice/css')

   mix
      .js('resources/frontoffice/js/app.js', 'frontoffice/js')
      .sass('resources/frontoffice/sass/app.scss', 'frontoffice/css')
   mix.browserSync({
       proxy: '127.0.0.1:8000'
   });