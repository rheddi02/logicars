<?php 
	if (! function_exists('sanitizer')) {
	    function sanitizer($request)
	    {
	        $values=array_map(function ($arr) {
	            if (!is_array($arr)) return trim(strip_tags($arr));
	        }, $request);

	        return $values;
	    }
	}
