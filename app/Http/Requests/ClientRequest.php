<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'=>['required','sometimes'],
            'lastname'=>['required','sometimes'],
            'email'=>'sometimes|required|unique:clients,email,'.$this->id,
            'password'=>['required','sometimes','confirmed'],
            'address'=>['required','sometimes'],
            'city'=>['required','sometimes'],
            'zip'=>['required','sometimes'],
            'telephone'=>['required','sometimes'],
        ];
    }

    public function messages()
    {
        return [
            'firstname.required' => ':attribute field is required'
        ];
    }

    public function attributes()
    {
        return [
            'email'=>'email address'
        ];
    }
}
