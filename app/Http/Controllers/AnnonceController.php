<?php

namespace App\Http\Controllers;

use App\Http\Requests\AnnonceRequest;
use App\Models\Annonce;
use App\Models\Item;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AnnonceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $keyword=request()->query('keyword');
        $filter=request()->get('filter');
        if ($keyword) {
            $page=Annonce::withAll()
            ->where('title','like','%'.$keyword.'%')
            ->orWhere('pick_address','like','%'.$keyword.'%')
            ->orWhereHas('client',function($q) use ($keyword){
                $q->where('firstname','like','%'.$keyword.'%');
            })
            ->orWhereHas('client',function($q) use ($keyword){
                $q->where('lastname','like','%'.$keyword.'%');
            })
            ->orderBy('title')->paginate($filter);
        } else {
            $page=Annonce::withAll()->orderBy('title')->paginate($filter);
        }
        return response()->json($page);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AnnonceRequest $request)
    {
        $this->createOrUpdate($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Annonce  $annonce
     * @return \Illuminate\Http\Response
     */
    public function show(Annonce $annonce)
    {
        $filter=request()->get('filter');
        $hint=request()->get('hint');
        $annonce=$annonce->load('status','client','annoncefiles','category');
        if ($hint) $$hint=$annonce->$hint()->withAll()->orderBy('created_at','desc')->paginate($filter);
        $data=(object)compact(
            $hint??'annonce'
        );
        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Annonce  $annonce
     * @return \Illuminate\Http\Response
     */
    public function edit(Annonce $annonce)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Annonce  $annonce
     * @return \Illuminate\Http\Response
     */
    public function update(AnnonceRequest $request, $id)
    {
        if ($request->has('is_public')) return response()->json(Annonce::find($id)->update(['is_public'=>$request->is_public]));
        $this->createOrUpdate($request,$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Annonce  $annonce
     * @return \Illuminate\Http\Response
     */
    public function destroy(Annonce $annonce)
    {
        $annonce->delete();
    }

    public function createOrUpdate($request, $id=null)
    {
        $annonce=Annonce::updateOrCreate(['id'=>$id],sanitizer($request->all()));
       
        extract($request->all());

        // modify related items
        if ($id) {
            $item_ids=array_column($items, 'id');
            $annonce->items()->whereNotIn('id',$item_ids)->delete();

            foreach ($items as $key => $value) {
                $annonce->items()->updateOrCreate(['id'=>$value['id']??0],sanitizer($value));
            }
        } else {
            foreach ($request->items as $item) {
                $annonce->items()->create(sanitizer($item));
            }
        }

        // modify related files
        if (isset($annoncefiles) && $annoncefiles) {
            $dir='public/files/'.$annonce->id.'/';

            $annoncefile_ids=array_column($annoncefiles, 'id');

            $annonce->annoncefiles()->whereNotIn('id',$annoncefile_ids)->delete();

            foreach ($annoncefiles as $annoncefile) {
                if (isset($annoncefile->id) && $annoncefile->id) {
                    $filename=Carbon::now()->timestamp.'_'.$annoncefile->getClientOriginalName();
                    $annoncefile->storeAs($dir,$filename);

                    $annonce->annoncefiles()->updateOrCreate(['id'=>$annoncefile->id??0 ],['filename'=>$filename ]);
                }
            }
        }
        return response()->json($annonce);    

    }
}
