<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Annonce;
use App\Models\Transporter;

class BidController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->createOrUpdate($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Annonce  $annonce
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->createOrUpdate($request,$id);
    }

    public function createOrUpdate($request, $id=null)
    {
    	$annonce=Annonce::firstOrNew(['id'=>$id]);

    	extract($request->all());

    	// check if annonce and transporter id 
    	$exists=$annonce->transporters->contains($transporter_id);

    	if ($exists) $annonce->transporters()->updateExistingPivot($transporter_id,['price'=>$price]); //update
    	else $annonce->transporters()->attach($transporter_id,['price'=>$price]); //create
    	
    	return response()->json($annonce);
    }
}
