<?php

namespace App\Http\Controllers;

use App\Http\Requests\MeasurementRequest;
use App\Models\Measure;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class MeasureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Measure::all();
        return response()->json($data);    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MeasurementRequest $request)
    {
        $this->createOrUpdate($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Measure  $measure
     * @return \Illuminate\Http\Response
     */
    public function show(Measure $measure)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Measure  $measure
     * @return \Illuminate\Http\Response
     */
    public function edit(Measure $measure)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Measure  $measure
     * @return \Illuminate\Http\Response
     */
    public function update(MeasurementRequest $request, $id)
    {
        $this->createOrUpdate($request,$id);
    }

    public function updateStatus(Request $request)
    {
        extract($request->all());
        return response()->json(Measure::find($id)->update(['status'=>$status]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Measure  $measure
     * @return \Illuminate\Http\Response
     */
    public function destroy(Measure $measure)
    {
        $measure->delete();    
    }

    public function createOrUpdate($request, $id=null)
    {
        extract($request->all());
        $measure=Measure::firstOrNew(['id' => $id]);
        $measure->name=$name;
        $measure->alias=$alias;
        $measure->save();

        return response()->json('success',200);    

    }
}
