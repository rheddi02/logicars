<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Models\Client;
use App\Models\Comment;
use App\Models\Transporter;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page=Comment::all();
        return response()->json($page);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request)
    {
        $this->createOrUpdate($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {   
        // $data=Comment::where('id',$comment->id)->withSort()->first();
        return response()->json($comment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(CommentRequest $request, $id)
    {
        $this->createOrUpdate($request,$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();
    }

    public function createOrUpdate($request, $id=null)
    {
        $comment=Comment::updateOrCreate(['id' => $id],$request->all());

        if (!$id) {
            if ($request->user_type) {
                $transporter=Transporter::find($request->user_id);
                $comment->transporters()->save($transporter);
            } else {
                $client=Client::find($request->user_id);
                $comment->clients()->save($client);
            }
        }

        return response()->json($comment);    

    }
}
