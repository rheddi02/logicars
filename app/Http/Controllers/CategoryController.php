<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filter=request()->get('filter');
        $page=Category::orderBy('name')->paginate($filter);
        return response()->json($page);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $this->createOrUpdate($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $filter=request()->get('filter');
        $hint=request()->get('hint');
        $category=$category->load('annonces');
        if ($hint) $$hint=$category->$hint()->withAll()->orderBy('created_at','desc')->paginate($filter);
        $data=(object)compact(
            $hint??'category'
        );
        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return $category;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
        if ($request->has('status')) return response()->json(Category::find($id)->update(['status'=>$request->status]));
        if ($request->has('file')) {
            $category=Category::find($id);
            Storage::delete('/public/'.$category->file);
        }
        $this->createOrUpdate($request,$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        Storage::delete('/public/'.$category->file);
        $category->delete();
    }

    public function createOrUpdate($request, $id=null)
    {
        $category=Category::updateOrCreate(['id'=>$id],sanitizer($request->all()));
       
        extract($request->all());

        if ($request->hasFile('file')) {
            $dir='public/icons/';
            $file=$request->file('file');
            $filename=Carbon::now()->timestamp.'_'.$file->getClientOriginalName();
            $file->storeAs($dir,$filename);
            $category->file='icons/'.$filename;
            $category->save();
        }

        return response()->json('success',200); 
    }
}
