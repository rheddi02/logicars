<?php
 
namespace App\Http\Controllers;
 
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Cookie;

class PassportController extends Controller
{
    /**
     * Handles Registration Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);
 
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
 
        $token = $user->createToken('logiCar')->accessToken;
 
        return response()->json(['token' => $token], 200);
    }
 
    /**
     * Handles Login Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {

        extract($request->all());

        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];
        
        if (isset($hint) && $hint) {
           if (Auth::guard($hint)->attempt($credentials)) {
                $token = Auth::guard($hint)->user()->createToken('logiCar',[$hint])->accessToken;
                return response()->json(['token' => $token,'user'=>Auth::guard($hint)->user()], 200);
            } else {
                return response()->json(['error' => 'Unauthorised'], 401);
            }
        } else {
            if (auth()->attempt($credentials)) {
                $token = auth()->user()->createToken('logiCar',['admin'])->accessToken;
                return response()->json(['token' => $token,'user'=>auth()->user()], 200);
            } else {
                return response()->json(['error' => 'Unauthorised'], 401);
            }
        }
    }   
 
    /**
     * Returns Authenticated User Details
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function details()
    {
        return response()->json(['user' => auth()->user()], 200);
    }

    public function logoutApi(Request $request)
	{ 
	    $request->user()->token()->delete();
        $cookie=Cookie::forget('route_token');
        return response()->json('success',200)->withCookie($cookie);
	}

    public function client_login(Request $request)
    {
        $request->validate([
            'email'=>'required|string|email',
            'password'=>'required|string',
            'remember_me'=>'boolean'
        ]);
        
        if (!Auth::guard('client')->attempt(['email'=>$request->email,'password'=>$request->password], $request->remember))
            return response()->json(['error'=>'Unauthorised'],401);

        $clientToken=Auth::guard('client')->user()->createToken('logiCar');
        $token=$clientToken->token;

        if ($request->remember_me) {
            $token->expires_at=Carbon::now()->addWeeks(1);
            $token->save();

            return response()->json([
                'access_token' => $clientToken->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse($clientToken->token->expires_at)->toDateTimeString()
            ]);
        }
    }
}