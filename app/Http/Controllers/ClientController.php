<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClientRequest;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;

class ClientController extends Controller
{

    public function getuser(Request $request)
    {
        Config::set('auth.guards.api.provider','clients');
        return Auth::guard('api')->user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $keyword=request()->query('keyword');
        $filter=request()->get('filter');
        if ($keyword) {
            $page=Client::where('firstname','like','%'.$keyword.'%')
            ->orWhere('lastname','like','%'.$keyword.'%')
            ->orWhere('alias','like','%'.$keyword.'%')
            ->orWhere('company','like','%'.$keyword.'%')
            ->orWhere('email','like','%'.$keyword.'%')
            ->orWhere('address','like','%'.$keyword.'%')
            ->orWhere('city','like','%'.$keyword.'%')
            ->orWhere('telephone','like','%'.$keyword.'%')
            ->orderBy('lastname')->paginate($filter);
        } else {
            $page=Client::orderBy('lastname')->paginate($filter);
        }
        return response()->json($page);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientRequest $request)
    {
        // $this->createOrUpdate($request);
        // $request->request->set('alias',strtoupper(substr($request->firstname,0,2)).substr(date_timestamp_get(date_create()),6,4));
        $sanitized = sanitizer($request->all()); // strips tags and trim

        $client=Client::create($sanitized);

        return response()->json($client);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        $filter=request()->get('filter');
        $annonces=$client->annonces()->paginate($filter);
        $data=(object)compact(
            'client',
            'annonces'
        );
        
        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(ClientRequest $request, Client $client)
    {
        // $this->createOrUpdate($request,$id);
        $sanitized = sanitizer($request->all()); // strips tags and trim
        
        if (array_key_exists('status', $sanitized)) {
            return $client->update(['status'=>$sanitized['status']]);
        }

        $client->update($sanitized);

        return response()->json($client);
    }

    public function updateStatus(Request $request)
    {
        extract($request->all());
        return response()->json(Client::find($id)->update(['status'=>$status]));
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $client->delete();
    }

    public function createOrUpdate($request, $id=null)
    {
        // $comment=Client::updateOrCreate(['id' => $id],$request->all());
        // $client=Client::firstOrNew(['id' => $id]);
        
        // foreach ($request->except(['_method','id','password_confirmation']) as $key => $value) {
        //     if ($key=='password') $client->$key=Hash::make($value);
        //     else $client->$key=$value;
        // }

        // extract content of request to access variable dirctly
        // extract($request->all());

        //  if create new
        // if (!$id) {
            // $id=DB::table('clients')->orderBy('id','desc')->first()->id;
            // if ($firstname || $lastname) $client->alias=strtoupper(substr($firstname??$lastname,0,2)).($id+1);
            // if ($firstname || $lastname) $client->alias=strtoupper(substr($firstname??$lastname,0,2)).substr(date_timestamp_get(date_create()),6,4);
        // }

        // $client->save();

        // return response()->json($client);    

    }
}
