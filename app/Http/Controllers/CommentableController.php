<?php

namespace App\Http\Controllers;

use App\Models\Commentable;
use Illuminate\Http\Request;

class CommentableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Commentable  $commentable
     * @return \Illuminate\Http\Response
     */
    public function show(Commentable $commentable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Commentable  $commentable
     * @return \Illuminate\Http\Response
     */
    public function edit(Commentable $commentable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Commentable  $commentable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Commentable $commentable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Commentable  $commentable
     * @return \Illuminate\Http\Response
     */
    public function destroy(Commentable $commentable)
    {
        //
    }
}
