<?php

namespace App\Http\Controllers;

use App\Http\Requests\TransporterRequest;
use App\Models\Transporter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;

class TransporterController extends Controller
{

    public function getuser(Request $request)
    {
        Config::set('auth.guards.api.provider','transporters');
        return Auth::guard('api')->user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $keyword=request()->query('keyword');
        $filter=request()->get('filter');
        if ($keyword) {
            $page=Transporter::withAll()
            ->where('firstname','like','%'.$keyword.'%')
            ->orwhere('lastname','like','%'.$keyword.'%')
            ->orWhere('company','like','%'.$keyword.'%')
            ->orderBy('lastname')->paginate($filter);
        } else {
            $page=Transporter::withAll()->orderBy('lastname')->paginate($filter);
        }
        return response()->json($page);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TransporterRequest $request)
    {
        // $this->createOrUpdate($request);
        // $request->request->set('alias',strtoupper(substr($request->firstname,0,2)).substr(date_timestamp_get(date_create()),6,4));
        $sanitized = sanitizer($request->all()); // strips tags and trim
        // $values=array_map(function($value) {
        //     return trim(strip_tags($value));
        // }, $request->all());
        $transporter=Transporter::create($sanitized);

        return response()->json($transporter);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transporter  $transporter
     * @return \Illuminate\Http\Response
     */
    public function show(Transporter $transporter)
    {
        $filter=request()->get('filter');
        $annonces=$transporter->annonces()->orderBy('id','desc')->paginate($filter);
        $data=(object)compact(
            'transporter',
            'annonces'
        );
        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transporter  $transporter
     * @return \Illuminate\Http\Response
     */
    public function edit(Transporter $transporter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transporter  $transporter
     * @return \Illuminate\Http\Response
     */
    public function update(TransporterRequest $request,Transporter $transporter)
    {
        // $values=array_map(function($value) {
        //     return trim(strip_tags($value));
        // }, $request->all());
        // $this->createOrUpdate($request,$id);

        $sanitized = sanitizer($request->all()); // strips tags and trim
        if (array_key_exists('is_verified', $sanitized)) {
            $transporter->update(['is_verified'=>$sanitized['is_verified']]);
            return response()->json($transporter);
        }

        if (array_key_exists('status', $sanitized)) {
            return $transporter->update(['status'=>$sanitized['status']]);
        }

        $transporter->update($sanitized);

        return response()->json($transporter);
    }

    public function updateStatus(Request $request)
    {
        extract($request->all());
        return response()->json(Transporter::find($id)->update(['status'=>$status]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transporter  $transporter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transporter $transporter)
    {
        $transporter->delete();
    }

    public function createOrUpdate($request, $id=null)
    {
        // $transporter=Transporter::firstOrNew(['id' => $id]);

        // foreach ($request->except(['_method','id','password_confirmation']) as $key => $value) {
        //     if ($key=='password') $transporter->$key=Hash::make($value);
        //     else $transporter->$key=$value;
        // }

        // // extract content of request to access variable dirctly
        // extract($request->all());

        // //  if create new
        // if (!$id) {
        //     // $id=DB::table('clients')->orderBy('id','desc')->first()->id;
        //     // if ($firstname || $lastname) $transporter->alias=strtoupper(substr($firstname??$lastname,0,2)).($id+1);
        //     if ($firstname || $lastname) $transporter->alias=strtoupper(substr($firstname??$lastname,0,2)).substr(date_timestamp_get(date_create()),6,4);
        // }

        // $transporter->save();

        // return response()->json($transporter);    

    }

    // public function sanitizer($request)
    // {
    //     $values=array_map(function($value) {
    //         return trim(strip_tags($value));
    //     }, $request);

    //     return $values;
    // }
}
