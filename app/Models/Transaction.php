<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

	public function annonce()
	{
		return $this->belongsTo(Annonce::class);
	}

	public function transporter()
	{
		return $this->belongsTo(Transporter::class,Annonce::class);
	}
	
    public function scopeWithAll($query)
    {
        $query->with('annonce');
    }
}
