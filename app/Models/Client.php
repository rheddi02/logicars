<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Client extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('annonces',function(Builder $builder) {
            $builder->withCount('annonces');
        });
    }

    protected $fillable=['id','firstname','lastname','alias','company','email','password','address','city','zip','telephone','logo','status'];
	protected $hidden=['created_at','remember_token','password','updated_at','is_client'];
    protected $appends = ['fulladdress','fullname'];
    
	public function annonces()
	{
		return $this->hasMany(Annonce::class);
	}

    public function factory()
    {
        Category::create([
            'name' => 'motorcycle',
            'slug' => 'motorcycle'
        ]);
        
        Status::create([
            'title' => 'pending'
        ]);

        Weight::create([
            'name' => 'kilogram',
            'alias' => 'kg',
        ]);

        Measure::create([
            'name' => 'meters',
            'alias' => 'm',
        ]);
        
    	return $client = factory(Client::class, 4)->create()
    		->each(function ($client){
    			$client->annonces()->createMany(
    				factory(Annonce::class,11)->make()->toArray()
    			);
    		});
    }

    public function comments()
    {
        return $this->morphToMany(Comment::class,'commentable');
    }

    public function getFullAddressAttribute()
    {
        return $this->address.', '.$this->city.' '.$this->zip;
    }

    public function getFullNameAttribute()
    {
        return ucfirst($this->lastname.', '.$this->firstname);
    }

    public function setFirstnameAttribute($value)
    {
        $this->attributes['firstname']=$value;
        if (request()->isMethod('post')) {
            $this->attributes['alias']=strtoupper(substr($value,0,2)).substr(date_timestamp_get(date_create()),6,4);
        }
    }
}
