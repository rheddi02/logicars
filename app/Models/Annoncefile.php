<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Annoncefile extends Model
{

	public static function boot()
	{
		parent::boot();

		static::deleting( function ($file) {
        	$file=base_path('storage/app/public/files/'.$file->annonce->id.'/'.$file->filename);
        	var_dump($file);
	        if (!file_exists($file) || is_dir($file)) abort(404,'Fichier non trouvé');
			unlink($file);
		});
	}

	protected $guarded=[];

    public function annonce()
    {
    	return $this->belongsTo(Annonce::class);
    }
}
