<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    // public function commentable()
    // {
    //     return $this->morphTo();
    // }

	protected static function boot()
	{
		parent::boot();

		static::addGlobalScope('order',function(Builder $builder) {
			$builder->orderBy('created_at','asc');
		});
	}

    // protected $guarded=[];
    protected $fillable=['id','comment','comment_id','status','annonce_id'];
	protected $with=[
		'clients',
		'transporters',
		'children'
	];
    
	public function clients()
	{
		return $this->morphedByMany(Client::class,'commentable');
	}

	public function transporters()
	{
		return $this->morphedByMany(Transporter::class,'commentable');
	}

    public function scopeWithAll($query)
    {
        $query->with('clients','transporters')->where('comment_id',0);
    }

    public function children()
    {
    	return $this->hasMany(Comment::class,'comment_id');
    }

    public function parent()
    {
    	return $this->hasOne(Comment::class,'id','comment_id');
    }

    public function getCommentAttribute($value)
    {
    	return ucfirst($value);
    }

    // public function getCreatedAtAttribute($value)
    // {
    //     return Carbon::parse($value)->format('d/m/Y H:i:s');
    // }
}
