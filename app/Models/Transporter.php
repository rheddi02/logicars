<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class Transporter extends Authenticatable
{
    use HasApiTokens, Notifiable;
    
    protected $fillable=['id','firstname','lastname','alias','company','description','email','password','address','city','zip','telephone','logo','status','is_verified'];
	protected $hidden=['remember_token','password','updated_at','status','is_transporter'];
	protected $appends = ['fulladdress','fullname'];

    public function factory()
    {
    	return $client = factory(Transporter::class,60)->create();
    }

    public function annonces()
    {
        return $this->belongsToMany(Annonce::class)->withPivot('price');
    }

    public function comments()
    {
        return $this->morphToMany(Comment::class,'commentable');
    }

    public function getFullAddressAttribute()
    {
    	return $this->address.', '.$this->city.' '.$this->zip;
    }

    public function getFullNameAttribute()
    {
    	return ucfirst($this->lastname.', '.$this->firstname);
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y');
    }

    public function scopeWithAll($query)
    {
        $query->with('comments','annonces');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password']=Hash::make($value);
    }

    public function setIsVerifiedAttribute($value)
    {
        $this->attributes['is_verified']=!$value?0:1;
    }

    public function setFirstnameAttribute($value)
    {
        $this->attributes['firstname']=$value;
        if (request()->isMethod('post')) {
            $this->attributes['alias']=strtoupper(substr($value,0,2)).substr(date_timestamp_get(date_create()),6,4);
        }
    }

}
