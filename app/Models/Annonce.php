<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Annonce extends Model
{
    // use SoftDeletes;
    // protected $guarded=[];
    protected $fillable=['title','description','pick_address','drop_address','distance','price_range','pick_date_from','pick_date_to','drop_date_from','drop_date_to','is_urgent','is_public','status_id','client_id','category_id'];
    protected $hidden=['updated_at'];
    
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function annoncefiles()
    {
    	return $this->hasMany(Annoncefile::class);
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }

    public function transporters()
    {
    	return $this->belongsToMany(Transporter::class)->withPivot('price')->withTimestamps();
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function getBidCountAttribute()
    {
        return $this->transporters()->count();
    }

    public function scopeWithAll($query)
    {
        $query->with('status','client','category','annoncefiles');
    }

    public function getPickDateRangeAttribute()
    {
        return $this->pick_date_from.' - '.$this->pick_date_to;
    }

    public function getDropDateRangeAttribute()
    {
        return $this->drop_date_from.' - '.$this->drop_date_to;
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y');
    }

    // public function getPickDateFromAttribute($value)
    // {
    //     return Carbon::parse($value)->format('d/m/Y');
    // }

    // public function getPickDateToAttribute($value)
    // {
    //     return Carbon::parse($value)->format('d/m/Y');
    // }

    // public function getDropDateFromAttribute($value)
    // {
    //     return Carbon::parse($value)->format('d/m/Y');
    // }

    // public function getDropDateToAttribute($value)
    // {
    //     return Carbon::parse($value)->format('d/m/Y');
    // }

    // public function setCreatedAtAttribute($value)
    // {
    //     $this->attributes['transaction_date'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    // }

    // public function setPickDateFromAttribute($value)
    // {
    //     $this->attributes['pick_date_from'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    // }

    // public function setPickDateToAttribute($value)
    // {
    //     $this->attributes['pick_date_to'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    // }
    
    // public function setDropDateFromAttribute($value)
    // {
    //     $this->attributes['drop_date_from'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    // }

    // public function setDropDateToAttribute($value)
    // {
    //     $this->attributes['drop_date_to'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    // }
}
