<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('annonces',function(Builder $builder) {
            $builder->withCount('annonces');
        });
    }
    
    use SoftDeletes;

    protected $fillable=['name','slug','file','description','status'];
    protected $dates = ['deleted_at'];
    protected $hidden=['created_at','updated_at'];
    
    public function getNameAttribute($value)
    {
    	return ucwords($value);
    }

	public function annonces()
	{
		return $this->hasMany(Annonce::class);
	}

    public function scopeWithAll($query)
    {
        $query->with('annonces');
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name']=$value;
        if (request()->isMethod('post')) {
            $this->attributes['slug']=str_replace(' ', '_', $value);
        }
    }

    public function setDescriptionAttribute($value)
    {
        $this->attributes['description']=$value==='undefined'?'':$value;
    }

    public function setFileAttribute($value)
    {
        $this->attributes['file']=$value==='undefined'?'':$value;
    }
}
