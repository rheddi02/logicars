<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    // protected $guarded=[];
    protected $fillable=['annonce_id','category_id','name','description','quantity','length','height','width','weigh','measure_id','weight_id'];
    protected $hidden=['created_at','updated_at'];

    public function annonce()
    {
    	return $this->belongsTo(Annonce::class);
    }

    public function weight()
    {
        return $this->belongsTo(Weight::class);
    } 

    public function measure()
    {
        return $this->belongsTo(Measure::class);
    }

    public function getHeightAttribute($value)
    {
    	if ($value) return $value.' '.$this->measure->alias;
    }

    public function getWidthAttribute($value)
    {
    	if ($value) return $value.' '.$this->measure->alias;
    }

    public function getLengthAttribute($value)
    {
    	if ($value) return $value.' '.$this->measure->alias;
    }

    public function getWeighAttribute($value)
    {
    	if ($value) return $value.' '.$this->weight->alias;
    }

    public function scopeWithAll($query)
    {
    	$query->with('annonce');
    }

    public function getNameAttribute($value)
    {
    	return ucwords($value);
    }
}
