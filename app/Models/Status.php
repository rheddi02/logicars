<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $guarded=[];

    public function annonces()
    {
    	return $this->hasMany(Annonce::class);
    }
}
