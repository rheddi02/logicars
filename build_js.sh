#!/bin/bash

OLD_RESOURCES_CHECKSUM="$(cat public/dev/resources_checksum)"
NEW_RESOURCES_CHECKSUM="$(find resources -type f -exec md5sum {} \; | md5sum)"

echo "Old checksum: $OLD_RESOURCES_CHECKSUM"
echo "New checksum: $NEW_RESOURCES_CHECKSUM"

if [ "$OLD_RESOURCES_CHECKSUM" != "$NEW_RESOURCES_CHECKSUM" ]
then

    echo "Need to rebuild js files"
    npm run dev
    EXIT_CODE=$?
    if [ "$EXIT_CODE" = "0" ]
    then

        echo "$NEW_RESOURCES_CHECKSUM" > public/dev/resources_checksum
        echo "Build success. New checksum saved."

    else

        echo "Build failed with exit code $EXIT_CODE"
        exit $EXIT_CODE

    fi

else

    echo "Checksum match. No need to rebuild."

fi