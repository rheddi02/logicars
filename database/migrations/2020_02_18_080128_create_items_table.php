<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('annonce_id');
            $table->string('name');
            $table->longtext('description');
            $table->integer('quantity')->nullable();
            $table->string('length')->nullable();
            $table->string('height')->nullable();
            $table->string('width')->nullable();
            $table->string('weigh')->nullable();
            $table->unsignedBigInteger('measure_id');
            $table->unsignedBigInteger('weight_id');
            
            $table->foreign('measure_id')
                ->references('id')->on('measures');
            $table->foreign('weight_id')
                ->references('id')->on('weights');
            $table->foreign('annonce_id')
                ->references('id')->on('annonces');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
