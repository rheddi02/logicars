<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            // $table->integer('commentable_id');
            // $table->string('commentable_type');
            // $table->integer('comment_id')->default(0);
            $table->longtext('comment');
            $table->integer('comment_id')->default(0);
            $table->integer('status')->default(1);
            $table->unsignedBigInteger('annonce_id');
            $table->foreign('annonce_id')
                ->references('id')->on('annonces');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
