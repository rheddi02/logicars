<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransportersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transporters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('alias');
            $table->string('company')->nullable();
            $table->string('description')->nullable();
            $table->string('email');
            $table->string('password');
            $table->string('address');
            $table->string('city');
            $table->string('zip');
            $table->string('telephone');
            $table->string('logo')->nullable();
            $table->boolean('is_transporter')->default(1);
            $table->rememberToken();
            $table->integer('status')->default(1);
            $table->boolean('is_verified')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transporters');
    }
}
