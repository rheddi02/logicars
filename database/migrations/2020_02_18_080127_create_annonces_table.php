<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnnoncesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annonces', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->longtext('description')->nullable();
            $table->string('pick_address');
            $table->string('drop_address');
            $table->string('distance')->nullable();
            $table->string('price_range')->nullable();
            $table->string('pick_date_from')->nullable();
            $table->string('pick_date_to')->nullable();
            $table->string('drop_date_from')->nullable();
            $table->string('drop_date_to')->nullable();
            $table->boolean('is_urgent')->default(0);
            $table->boolean('is_public')->default(1);
            $table->unsignedBigInteger('category_id');
            $table->softDeletes();
            
            $table->unsignedBigInteger('status_id');
            $table->unsignedBigInteger('client_id');

            $table->foreign('status_id')
                ->references('id')->on('statuses');
            $table->foreign('client_id')
                ->references('id')->on('clients');
            $table->foreign('category_id')
                ->references('id')->on('categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('annonces');
    }
}
