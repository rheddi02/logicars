<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnnonceTransporterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annonce_transporter', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('annonce_id');
            $table->unsignedBigInteger('transporter_id');
            $table->integer('price');
            $table->integer('status')->default(1);

            $table->foreign('annonce_id')
                ->references('id')->on('annonces');
            $table->foreign('transporter_id')
                ->references('id')->on('transporters');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('annonce_transporter');
    }
}
