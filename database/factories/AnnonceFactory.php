<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Annonce;
use Faker\Generator as Faker;

$factory->define(Annonce::class, function (Faker $faker) {
    return [
        'title' => $faker->jobTitle,
        'description' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'pick_address' => $faker->address,
        'drop_address' => $faker->address,
        'pick_date_from' => $faker->dateTimeBetween($startDate = 'now', $endDate = '+30 days', $timezone = null),
        'pick_date_to' => $faker->dateTimeBetween($startDate = 'now', $endDate = '+60 days', $timezone = null),
        'status_id' => 1,
    ];
});
