<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Client;
use Faker\Generator as Faker;

$factory->define(Client::class, function (Faker $faker) {
    return [
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'alias' => $faker->userName,
        'company' => $faker->company,
        'email' => $faker->unique()->safeEmail,
        'address' => $faker->streetName,
        'city' => $faker->city,
        'zip' => $faker->postcode,
        'telephone' => $faker->e164PhoneNumber,
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
        'status'=>1
    ];
});
