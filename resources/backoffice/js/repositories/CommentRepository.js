import AdminbaseURL from './Admin/axiosAdmin.js'

const baseURL = AdminbaseURL //api
const resource = '/comments';

export default {
    // invoke index method
    get(params = '', filter = '') {
        return axios.get(`${baseURL}${resource}/?page=`+params+'&filter='+filter)
    },

    // invoke show method
    show(id) {
        return axios.get(`${baseURL}${resource}/`+id)
    },

    // invoke create method
    fetch() {
        return axios.get(`${baseURL}${resource}/create`)
    },

    // invoke edit method
    getCity(id) {
        return axios.get(`${baseURL}${resource}/${id}/edit`);
    },

    // invoke store method
    create(payload) {
        return axios.post(`${baseURL}${resource}`, payload)
    },

    // invoke update method
    update(payload, id) {
        return axios.put(`${baseURL}${resource}/${id}`, payload);
    },

    //invoke delete method
    delete(id) {
        return axios.delete(`${baseURL}${resource}/${id}`)
    },

    fetchResources(){
        return axios.get(`${baseURL}${resource}/fetchResources`)
    }

    // MANY OTHER ENDPOINT RELATED STUFFS
};