import annonces from "./AnnonceRepository"
import categories from "./CategoryRepository"
import clients from "./ClientRepository"
import comments from "./CommentRepository"
import measures from "./MeasureRepository"
import transactions from "./TransactionRepository"
import transporters from "./TransporterRepository"
import weights from "./WeightRepository"

const repositories={
	'annonces':annonces,
	'categories':categories,
	'clients':clients,
	'comments':comments,
	'measures':measures,
	'transactions':transactions,
	'transporters':transporters,
	'weights':weights,
}

export default {
	get: name => repositories[name]
}