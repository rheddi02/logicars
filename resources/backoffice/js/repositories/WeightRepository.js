import AdminbaseURL from './Admin/axiosAdmin.js'

const baseURL = AdminbaseURL //api
const resource = '/weights';

export default {
    // invoke index method
    get(params = '', filter = '', keyword='') {
        return axios.get(`${baseURL}${resource}/?page=`+params+'&filter='+ filter+'&keyword='+keyword)
    },

    // invoke show method
    show(id, params = '', filter = '', hint = '') {
        return axios.get(`${baseURL}${resource}/`+id+'?page='+params+'&filter='+filter+'&hint='+hint)
    },

    // invoke create method
    fetch() {
        return axios.get(`${baseURL}${resource}/create`)
    },

    // invoke edit method
    getCity(id) {
        return axios.get(`${baseURL}${resource}/${id}/edit`);
    },

    // invoke store method
    create(payload) {
        return axios.post(`${baseURL}${resource}`, payload)
    },

    // invoke update method
    update(payload, id) {
        return axios.put(`${baseURL}${resource}/${id}`, payload);
    },

    updateStatus(payload) {
        return axios.post(`${baseURL}${resource}/updatestatus`, payload);
    },
    //invoke delete method
    delete(id) {
        return axios.delete(`${baseURL}${resource}/${id}`)
    },

    fetchResources(){
        return axios.get(`${baseURL}${resource}/fetchResources`)
    }

    // MANY OTHER ENDPOINT RELATED STUFFS
};