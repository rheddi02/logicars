require('./bootstrap');

window.Vue = require('vue');

import router from './router'
import vuetify from './src/plugins/vuetify' // path to vuetify export
import pagehelper from './mixins/PageHelper.vue'
import {store} from './src/store/index.js'
import VueCookies from 'vue-cookies'
// auto-import components
const files = require.context('./components/', true, /\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('main-app', require('./layout/Main.vue').default);
Vue.component('alert-dialog', require('./components/common/AlertDialog.vue').default);
Vue.component('delete-dialog', require('./components/common/DeleteDialog.vue').default);
Vue.component('status-dialog', require('./components/common/StatusDialog.vue').default);
Vue.mixin(pagehelper);
Vue.use(require('vue-moment'));
Vue.use(VueCookies)

axios.interceptors.request.use( (config) => {
    let token = $cookies.get('route_token')
    if (token) config.headers.Authorization='Bearer '+token
    store.commit('CHANGE_BTN_LOADER',true)
    store.commit('CHANGE_STATUS_LOADER',true)
    return config
})

axios.interceptors.response.use( ( response ) => {
    store.commit('CHANGE_LOADER',false)
    store.commit('CHANGE_STATUS_LOADER',false)
    store.commit('CHANGE_DELETE',false)
    store.commit('SHOW_ALERT',{visibility:0})
    return response;
}, ( error ) => {
    let message = ''
    status = 2

    switch (error.response.status) {
        case 404:
            message = 'nous n\'avons pas trouvé cette page'
            break;
        case 500:
            message = 'Erreur interne du serveur. Veuillez contacter le développeur pour résoudre ce problème.'
            break;
        case 503:
            message = 'Service indisponible, veuillez vérifier votre connexion internet pour continuer.'
            break;
        case 401:
            message = 'Désolé, vous n\'êtes pas authentifié pour faire cette action'
            break;
        case 403:
            status = 1
            message = 'Désolé, Vous n\'êtes pas autorisé pour cette action!'
            break;
        case 422:
            for (var key in error.response.data.errors) {
                message += ' '+error.response.data.errors[key];
            }
            break;
        default:
            message = 'Oups, quelque chose s\'est mal passé lors du traitement de votre demande'
            break;
    }
    store.commit('CHANGE_BTN_LOADER',false)
    store.commit('CHANGE_STATUS_LOADER',false)
    store.commit('SHOW_ALERT',{visibility:1,message:message,status:status})    
    return Promise.reject( error );
});

const app = new Vue({
    el: '#app',
    router,
    vuetify,
    store,
    mounted() {
        if (!localStorage.getItem('annoncePerPage')) localStorage.setItem('annoncePerPage',10)
        if (!localStorage.getItem('clientPerPage')) localStorage.setItem('clientPerPage',10)
        if (!localStorage.getItem('transactionPerPage')) localStorage.setItem('transactionPerPage',10)
        if (!localStorage.getItem('transporterPerPage')) localStorage.setItem('transporterPerPage',10)
    },
    created() {
        this.$store.dispatch('fetchCurrentUser')
    }
});


