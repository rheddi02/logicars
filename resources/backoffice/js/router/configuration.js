import index from '@backoffice/pages/Configurations/Layout'

export default [
    {
        path: '/configurations',
        name: 'configurations',
        component: index
    }
]