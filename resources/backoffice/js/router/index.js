import Vue from 'vue'
import VueRouter from 'vue-router'

import annonces from './annonces'
import categories from './categories'
import clients from './clients'
import configuration from './configuration'
import dashboard from './dashboard'
import login from './login'
import transactions from './transactions'
import transporters from './transporters'
import users from './users'

Vue.use(VueRouter)

const routes = [
	...annonces,
	...categories,
	...clients,
	...configuration,
	...dashboard,
	...login,
	...transactions,
	...transporters,
	...users,
]
	
const router = new VueRouter({
	base: '/admin',
	mode: 'history',
	routes,
})

export default router