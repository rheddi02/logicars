import Layout from '@backoffice/pages/Transaction/Layout'
import Index from '@backoffice/pages/Transaction/Index'
import Add from '@backoffice/pages/Transaction/Add'
import Show from '@backoffice/pages/Transaction/Show'
import Edit from '@backoffice/pages/Transaction/Edit'

export default [
    {
        path: '/transactions',
        name: 'transactions',
        component: Index,
    },
    {
        path: '/transactions/:id',
        name: 'showTransactions',
        component: Show
    },
    {
        path: '/transactions/:id/edit',
        name: 'editTransactions',
        component: Edit
    },
    {
        path: '/transactions/create',
        name: 'addTransactions',
        component: Add
    },
    
]