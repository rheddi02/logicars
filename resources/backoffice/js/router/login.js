import Layout from '@backoffice/pages/Login/Layout'

export default [
    {
        path: '/login',
        name:'login',
        component: Layout
    }
]