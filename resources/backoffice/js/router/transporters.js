import Layout from '@backoffice/pages/Transporters/Layout'
import Index from '@backoffice/pages/Transporters/Index'
import Add from '@backoffice/pages/Transporters/Add'
import Show from '@backoffice/pages/Transporters/Show'
import Edit from '@backoffice/pages/Transporters/Edit'

export default [
    {
        path: '/transporters',
        name: 'transporters',
        component: Index,
    },
    {
        path: '/transporters/:id',
        name: 'showTransporters',
        component: Show
    },
    {
        path: '/transporters/:id/edit',
        name: 'editTransporters',
        component: Edit
    },
    {
        path: '/transporters/create',
        name: 'addTransporters',
        component: Add
    },
]
