import index from '@backoffice/pages/Dashboard/Index'

export default [
    {
        path: '/dashboard',
        name: 'dashboard',
        component: index
    }
]