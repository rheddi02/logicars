import changepassword from '@backoffice/pages/Users/ChangePassword'

export default [
    {
        path: '/changepassword',
        name: 'changepassword',
        component: changepassword,
    },
]
