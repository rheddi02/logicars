import Layout from '@backoffice/pages/Annonces/Layout'
import Index from '@backoffice/pages/Annonces/Index'
import Add from '@backoffice/pages/Annonces/Add'
import Show from '@backoffice/pages/Annonces/Show'
import Edit from '@backoffice/pages/Annonces/Edit'

export default [
    {
        path: '/annonces',
        name: 'annonces',
        component: Index,
    },
	{
        path: '/annonces/:id/edit',
        name: 'editAnnonces',
        component: Edit
    },
    {
        path: '/annonces/:id',
        name: 'showAnnonces',
        component: Show
    },
    {
        path: '/annonces/create',
        name: 'addAnnonces',
        component: Add
    },
    
]
