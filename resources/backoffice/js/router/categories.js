import Layout from '@backoffice/pages/Categories/Layout'
import Index from '@backoffice/pages/Categories/Index'
import Add from '@backoffice/pages/Categories/Add'
import Show from '@backoffice/pages/Categories/Show'
import Edit from '@backoffice/pages/Categories/Edit'

export default [
    {
        path: '/categories',
        name: 'categories',
        component: Index,
    },
	{
        path: '/categories/create',
        name: 'addCategories',
        component: Add
    },
    {
        path: '/categories/:id',
        name: 'showCategories',
        component: Show
    },
    {
        path: '/categories/:id/edit',
        name: 'editCategories',
        component: Edit
    },

]