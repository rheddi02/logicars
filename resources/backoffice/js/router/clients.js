import Layout from '@backoffice/pages/Clients/Layout'
import Index from '@backoffice/pages/Clients/Index'
import Add from '@backoffice/pages/Clients/Add'
import Show from '@backoffice/pages/Clients/Show'
import Edit from '@backoffice/pages/Clients/Edit'

export default [
    {
        path: '/clients',
        name: 'clients',
        component: Index,
    },
    {
        path: '/clients/:id',
        name: 'showClients',
        component: Show
    },
	{
        path: '/clients/:id/edit',
        name: 'editClients',
        component: Edit
    },
    {
        path: '/clients/create',
        name: 'addClients',
        component: Add
    },
    
]