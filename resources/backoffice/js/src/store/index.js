import Vue from 'vue'
import Vuex from 'vuex'
import general from './modules/general'
import user from './modules/user'

Vue.use(Vuex);


export const store = new Vuex.Store({
    strict:true,
    modules: {
        general,
        user,
    }
});