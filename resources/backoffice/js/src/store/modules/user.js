export default {
    state: {
        user:null,
    },
    getters: {
        is_authenticated: state => state.user?true:false
    },
    mutations: {
        setUser(state,user) {
            state.user = user
        }
    },
    actions: {
        authenticateUser(state,data) {
            return new Promise((resolve, reject) => {
                axios.post('/api/login',data)
                    .then(({data})=>{
                        state.user=data.user
                        $cookies.set('route_token',data.token)
                        resolve({name:'dashboard'})
                    })
                    .catch(data=>{
                        reject()
                    })
                    
            })
        },
        fetchCurrentUser({ commit, dispatch }) {
            let route_token=$cookies.get('route_token')
            if (route_token) {
                axios.get('/api/user')
                    .then(({data})=>{
                        commit('setUser',data)
                    })
            }
        },
        
    },
}