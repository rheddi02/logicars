export default {
    state: {
        left_sidebar:true,
        btnloader:false,
        statusloader:false,
        loading2: true,
        delete: false,
        alert: {
            visibility:false,
            message:'',
            status:0,
        },
        message_alert:{
            visibility:false,
            message:'',
            status:0,
        },
    },
    getters: {
        left_sidebar_status: state => state.left_sidebar,
        loading2: state => state.loading2,
        btnloader_status: state => state.btnloader,
        statusloader_status: state => state.statusloader,
        delete: state => state.delete,
        alert: state => state.alert,
        message_alert: state => state.message_alert,
    },
    mutations: {
        CHANGE_LEFT_SIDEBAR(state) {
            state.left_sidebar = !state.left_sidebar
        },
        CHANGE_LOADER(state,status) {
            state.loading2 = status
        },
        CHANGE_BTN_LOADER(state,status) {
            state.btnloader = status
        },
        CHANGE_STATUS_LOADER(state,status) {
            state.statusloader = status
        },
        CHANGE_DELETE(state,status) {
            state.delete = status
        },
        SHOW_ALERT(state,alert) {
            state.alert = alert
        },  
        SHOW_MESSAGE_ALERT(state,alert) {
            state.message_alert = alert
        }       
    },
    actions: {
       
    }
}