import Vue from 'vue'
import VueRouter from 'vue-router';

import account from './account';
import dashboard from './dashboard';
import login from './login';

Vue.use(VueRouter);

const routes = [
	account,
	dashboard,
	login,
]
	
const router = new VueRouter({
		mode: 'history',
		routes
});

export default router;