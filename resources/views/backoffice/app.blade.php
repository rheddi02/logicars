<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Fonts -->
    <link href="{{ asset('dev/backoffice/css/font.css', config('app.manifest_path')) }}" rel="stylesheet">
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet"> -->
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
<!--      <link rel="dns-prefetch" href="//fonts.gstatic.com"> 
     <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">  -->
    <!-- Styles -->
    <link href="{{ mix('backoffice/css/app.css', config('app.manifest_path')) }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <main-app></main-app>
    </div>
    <script src="{{ mix('backoffice/js/app.js', config('app.manifest_path')) }}"></script>
    <script> 
        var asset = "{{asset('storage')}}"
    </script>
</body>
</html>
