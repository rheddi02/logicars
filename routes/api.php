<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::post('register','PassportController@register');

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//login with hint
Route::post('login','PassportController@login');

//get client/transporter details
Route::get('/client/user','ClientController@getuser');
Route::get('/transporter/user','TransporterController@getuser');

Route::group(['middleware'=>['auth:api']],function(){

	//get admin user details
	Route::get('/user',function(Request $request){
		return $request->user();
	});

	// update statuses
	// Route::post('annonce/updatestatus','AnnonceController@updateStatus');
	// Route::post('categories/updatestatus','CategoryController@updateStatus');
	Route::post('clients/updatestatus','ClientController@updateStatus');
	Route::post('measures/updatestatus','MeasureController@updateStatus');
	Route::post('transporters/updatestatus','TransporterController@updateStatus');
	Route::post('weights/updatestatus','WeightController@updateStatus');

	Route::resources([
		'annonces'=>'AnnonceController',
		'bids'=>'BidController',
		'categories'=>'CategoryController',
		'clients'=>'ClientController',
		'comments'=>'CommentController',
		'measures'=>'MeasureController',
		'transactions'=>'TransactionController',
		'transporters'=>'TransporterController',
		'weights'=>'WeightController',
		'users'=>'UserController',
	]);

	Route::post('logout','PassportController@logoutApi');
});